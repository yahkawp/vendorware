#!/bin/sh


bins=$HOME/bins

[ -d "$bins" ] || mkdir $bins

rm -rf $bins/vendor

ln -s $(pwd)/vendor $bins/
